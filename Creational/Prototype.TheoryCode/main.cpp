#include <iostream>
#include <string>
#include <stdexcept>
#include <map>
#include "prototype.hpp"

using namespace std;

class PrototypeManager
{
    using PrototypeMap = map<string, shared_ptr<Prototype>>;
    PrototypeMap prototypes_;
public:
    PrototypeManager()
    {}

    void insert(const string& id, shared_ptr<Prototype> prototype)
    {
        prototypes_.insert(make_pair(id, prototype));
    }

    shared_ptr<Prototype> find(const string& id)
    {
        auto it = prototypes_.find(id);

        if (it != prototypes_.end())
            return it->second;


        throw runtime_error("Unknown Id");
    }
};

int main()
{
    PrototypeManager manager;

    shared_ptr<Prototype> p1 = make_shared<ConcretePrototype1>("State A");
    shared_ptr<Prototype> p2 = make_shared<ConcretePrototype1>("State B");
    shared_ptr<Prototype> p3 = make_shared<ConcretePrototype2>("State A");
    shared_ptr<Prototype> p4 = make_shared<ConcretePrototype2>("State B");

    manager.insert("P1A", p1);
    manager.insert("P1B", p2);
    manager.insert("P2A", p3);
    manager.insert("P2B", p4);

    shared_ptr<Prototype> found1 = manager.find("P2B");

    unique_ptr<Prototype> cloned1 = found1->clone();

    cloned1->run();
}
