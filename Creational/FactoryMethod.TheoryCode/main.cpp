#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

namespace Canonical
{
    class Client
    {
        shared_ptr<ServiceCreator> creator_;
    public:
        Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
        {
        }

        Client(const Client&) = delete;
        Client& operator=(const Client&) = delete;

        void use()
        {
            unique_ptr<Service> service = creator_->create_service();

            string result = service->run();
            cout << "Client is using: " << result << endl;
        }
    };
}

class Client
{
    ServiceCreator creator_;
public:
    Client(const ServiceCreator& creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

typedef std::map<std::string, ServiceCreator> Factory;

int main()
{
    Factory creators;
    creators.insert(make_pair("ServiceA", &make_unique<ConcreteServiceA>));
    creators.insert(make_pair("ServiceB", [] { return make_unique<ConcreteServiceB>(); }));
    creators.insert(make_pair("ServiceC", &make_unique<ConcreteServiceC>));

    Client client(creators["ServiceC"]);
    client.use();
}
