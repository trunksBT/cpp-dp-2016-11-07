#include "square_reader_writer.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace IO;

namespace
{
    bool is_registered =
            SingtonShapeRWFactory::instance()
                .register_creator(type_index(typeid(Square)), &make_unique<SquareReaderWriter>);
}

void Drawing::IO::SquareReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Square& sqr = static_cast<Square&>(shp);

    Point pt;
    int size;

    in >> pt >> size;

    sqr.set_size(size);
    sqr.set_coord(pt);
}

void Drawing::IO::SquareReaderWriter::write(Drawing::Shape& shp, std::ostream& out)
{
    Square& square = dynamic_cast<Drawing::Square&>(shp);

    out << square.id << " " << square.coord() << " " << square.size() << std::endl;
}
