#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
	Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();
}
