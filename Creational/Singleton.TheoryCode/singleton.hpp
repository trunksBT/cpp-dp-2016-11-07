#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>


class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
        if (!instance_)
        {
            std::lock_guard<std::mutex> lk{mtx_};

            if (!instance_)
            {
                //instance_ = new Singleton();
                // 1 - alokacja
                void* raw_mem = ::operator new(sizeof(Singleton));

                // 2 - placement new
                new (raw_mem) Singleton();

                // 3 -assignment
                instance_ = static_cast<Singleton*>(raw_mem); // xxxxx
            }
        }

		return *instance_;
	}

	void do_something();

private:
    static std::atomic<Singleton*> instance_;  // uniqueInstance
    static std::mutex mtx_;
	
	Singleton() // disallows creation of new instances outside the class
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	~Singleton()
	{ 
		std::cout << "Singleton has been destroyed!" << std::endl;
	} 
};

std::atomic<Singleton*> Singleton::instance_;
std::mutex Singleton::mtx_;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
