#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>
#include <unordered_map>

#include "shape.hpp"
#include "rectangle.hpp"
#include "square.hpp"
#include "shape_readers_writers/rectangle_reader_writer.hpp"
#include "shape_readers_writers/square_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

unique_ptr<Shape> create_shape(const string& id)
{
    if (id == Rectangle::id)
        return make_unique<Rectangle>();
    else if (id == Square::id)
        return make_unique<Square>();

    throw runtime_error("Unknown shape id");
}

class ShapeCreator
{
public:
    virtual ~ShapeCreator() = default;
    virtual std::unique_ptr<Shape> create() = 0;
};

//class RectangleCreator : public ShapeCreator
//{
//    // ShapeCreator interface
//public:
//    std::unique_ptr<Shape> create() override
//    {
//        return make_unique<Rectangle>();
//    }
//};

//class SquareCreator : public ShapeCreator
//{
//    // ShapeCreator interface
//public:
//    std::unique_ptr<Shape> create() override
//    {
//        return make_unique<Square>();
//    }
//};

template <typename ShapeType>
class GenericShapeCreator : public ShapeCreator
{
    // ShapeCreator interface
public:
    std::unique_ptr<Shape> create() override
    {
        return make_unique<ShapeType>();
    }
};

class ShapeFactory
{
    using ShapeCreatorPtr = std::unique_ptr<ShapeCreator>;  // typedef

    std::unordered_map<std::string, ShapeCreatorPtr> creators_;
public:
    bool register_creator(const std::string& id, ShapeCreatorPtr creator)
    {
        std::unordered_map<std::string, ShapeCreatorPtr>::iterator it;
        bool is_inserted;

        tie(it, is_inserted) = creators_.insert(make_pair(id, move(creator)));

        return is_inserted;
    }

    std::unique_ptr<Shape> create(const string& id)
    {
        auto& creator = creators_.at(id);

        return creator->create();
    }
};

unique_ptr<ShapeReaderWriter> create_shape_rw(Shape& shape)
{
    if (typeid(shape) == typeid(Rectangle))
        return make_unique<RectangleReaderWriter>();
    else if (typeid(shape) == typeid(Square))
        return make_unique<SquareReaderWriter>();

    throw runtime_error("Unknown shape id");
}

class GraphicsDoc
{
    vector<unique_ptr<Shape>> shapes_;
    ShapeFactory& shape_factory_;
public:
    GraphicsDoc(ShapeFactory& shape_factory) : shape_factory_{shape_factory}
    {}


    void add(unique_ptr<Shape> shp)
    {
        shapes_.push_back(move(shp));
    }

    void render()
    {
        for(const auto& shp : shapes_)
            shp->draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        while (file_in)
        {
            string shape_id;
            file_in >> shape_id;        

            if (!file_in)
                return;

            cout << "Loading " << shape_id << "..." << endl;

            auto shape = shape_factory_.create(shape_id);
            auto shape_rw = create_shape_rw(*shape);

            shape_rw->read(*shape, file_in);

            shapes_.push_back(move(shape));
        }
    }

    void save(const string& filename)
    {
        ofstream file_out{filename};

        for(const auto& shp : shapes_)
        {
            auto shape_rw = create_shape_rw(*shp);
            shape_rw->write(*shp, file_out);
        }
    }
};

void bootstrap(ShapeFactory& shape_factory)
{
    shape_factory.register_creator(Rectangle::id, make_unique<GenericShapeCreator<Rectangle>>());
    shape_factory.register_creator(Square::id, make_unique<GenericShapeCreator<Square>>());
}

int main()
{
    ShapeFactory shape_factory;
    bootstrap(shape_factory);

    cout << "Start..." << endl;

    GraphicsDoc doc(shape_factory);

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    doc.save("new_drawing.txt");
}
