#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>
#include <unordered_map>
#include <functional>
#include <typeindex>

#include "shape.hpp"
#include "rectangle.hpp"
#include "square.hpp"
#include "shape_readers_writers/rectangle_reader_writer.hpp"
#include "shape_readers_writers/square_reader_writer.hpp"
#include "generic_factory.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

using ShapeFactory = GenericFactory<Shape>;
using ShapeRWFactory = GenericFactory<ShapeReaderWriter, std::type_index>;

type_index create_type_index(Shape& shape)
{
    return type_index(typeid(shape));
}

class GraphicsDoc
{
    vector<unique_ptr<Shape>> shapes_;
    ShapeFactory& shape_factory_;
    ShapeRWFactory& shape_rw_factory_;
public:
    GraphicsDoc(ShapeFactory& shape_factory, ShapeRWFactory& shape_rw_factory)
            : shape_factory_{shape_factory}, shape_rw_factory_{shape_rw_factory}
        {}


    void add(unique_ptr<Shape> shp)
    {
        shapes_.push_back(move(shp));
    }

    void render()
    {
        for(const auto& shp : shapes_)
            shp->draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        while (file_in)
        {
            string shape_id;
            file_in >> shape_id;        

            if (!file_in)
                return;

            cout << "Loading " << shape_id << "..." << endl;

            auto shape = shape_factory_.create(shape_id);
            auto shape_rw = shape_rw_factory_.create(create_type_index(*shape));

            shape_rw->read(*shape, file_in);

            shapes_.push_back(move(shape));
        }
    }

    void save(const string& filename)
    {
        ofstream file_out{filename};

        for(const auto& shp : shapes_)
        {
            auto shape_rw = shape_rw_factory_.create(create_type_index(*shp));
            shape_rw->write(*shp, file_out);
        }
    }
};

void bootstrap(ShapeFactory& shape_factory)
{
    shape_factory.register_creator(Rectangle::id, [] { return make_unique<Rectangle>(); });
    shape_factory.register_creator(Square::id, &make_unique<Square>);
}

void bootstrap(ShapeRWFactory& shape_rw_factory)
{
    shape_rw_factory.register_creator(
                type_index(typeid(Rectangle)), [] { return make_unique<RectangleReaderWriter>(); });

    shape_rw_factory.register_creator(
                type_index(typeid(Square)), &make_unique<SquareReaderWriter>);
}

ShapeFactory shape_factory;

int main()
{
    ShapeFactory shape_factory;
    ShapeRWFactory shape_rw_factory;

    bootstrap(shape_factory);
    bootstrap(shape_rw_factory);

    cout << "Start..." << endl;

    GraphicsDoc doc(shape_factory, shape_rw_factory);

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    doc.save("new_drawing.txt");
}
